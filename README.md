# gRPC Book Finder

[![build status](https://gitlab.com/BKer/grpc-book-finder-example/badges/master/build.svg)](https://gitlab.com/BKer/grpc-book-finder-example/commits/master)

This project is a demo project as part of an article written for the NLJUG Java Magazine. You will be able to search and
find books by giving an author. The server will reply with a set of books if found.

## Getting Started

In this section you will find how to get started with the project and what the prerequisites of running the project are.
You can checkout the project as follows:

```
git clone https://gitlab.com/BKer/grpc-book-finder-example.git
```

## Prerequisites

* Java >= 1.8

Given the article is written as part of the NLJUG Java Magazine no installation instructions are provided and a working
installation of Java is assumed.

## Building

In the project folder you will find a Gradle Wrapper. You can use this to build the project. Execute the following:

```
./gradlew clean build serverJar clientJar
```

or

```
./gradlew.bat clean build serverJar clientJar
```

## Running the server

```java -jar build/libs/grpc-server-1.0.0-SNAPSHOT.jar```

If all went well the output will contain the following two lines:

```
INFO: Creating server on port: 1804
INFO: Server started
```

## Running the client

```java -jar build/libs/grpc-client-1.0.0-SNAPSHOT.jar```

If all went well you will be greated by a menu of options. The client will execute the given option and display the menu
again once execution has finished.

## Build With

* [Gradle](https://gradle.org/) - Build and depedency management
* [gRPC](https://grpc.io/) - Libraries to use gRPC
* [Protobuf](https://developers.google.com/protocol-buffers/) - Protocol used

## Authors

* Bart Kerkvliet - Initial Work - [Quintor](https://www.quintor.nl)

## License

This project is licensed under the MIT License - see the [LICENSE.md](./LICENSE.md) file for details.
