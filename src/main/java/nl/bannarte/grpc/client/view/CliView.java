package nl.bannarte.grpc.client.view;

import nl.bannarte.grpc.Book;
import nl.bannarte.grpc.BookList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

/**
 * A Command Line Interface for the gRPC demo.
 * <p>
 * Simple CLI for the demo. Will print options and allow for numeric input.
 *
 * @author Bart Kerkvliet
 * @version 1.0.0
 */
public class CliView extends Observable implements Observer {
    /**
     * Logger used to generate log lines.
     */
    private static final Logger logger = Logger.getLogger(CliView.class.getName());

    /**
     * Updates received will be handled here.
     * <p>
     * Will either print a received value or when the service is done present a new options screen.
     *
     * @param o   the observable object.
     * @param arg an argument passed.
     * @see java.util.Observer
     * @see java.util.Observable
     */
    @Override
    public void update(Observable o, Object arg) {
        logger.fine("Received an update" + arg);
        if (arg instanceof Boolean) {
            Boolean isDone = (Boolean) arg;
            if (isDone) {
                showOptions();
                getInput();
            }
        } else if (arg instanceof BookList) {
            BookList bookList = (BookList) arg;
            System.out.println(bookList);
        } else if (arg instanceof Book) {
            Book book = (Book) arg;
            System.out.println(book);
        } else if (arg instanceof String) {
            String str = (String) arg;
            if (str.equals("EXIT")) {
                logger.info("Going for shutdown");
                System.exit(0);
            }
        } else {
            logger.severe("Unexpected update, shutting down");
            System.exit(1);
        }
    }

    /**
     * Show the possible options.
     */
    private void showOptions() {
        System.out.println(new StringBuilder()
                .append("Options:\n")
                .append("  1: Get Books (BookList) Blocking\n")
                .append("  2: Get Books Blocking, but use the stream\n")
                .append("  3: Get Books (BookList) Blocking, but use an unknown author\n")
                .append("  4: Get Books Blocking, but set a deadline\n")
                .append("  5: Get Books Blocking, but use the stream and set a deadline\n")
                .append("---\n").append("  6: Get Books (BookList) asynchronously\n")
                .append("  7: Get Books asynchronously, but use the stream\n")
                .append("  8: Get Books asynchronously, but use the stream and set a deadline\n")
                .append("---\n").append("  9: Get Books (BookList) as a Future\n")
                .append("---\n").append("  0: Exit\n")
                .toString());
    }

    /**
     * Get the input and broadcast the new input.
     */
    private void getInput() {
        try {
            System.out.println("Enter Option: ");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String input = br.readLine();
            setChanged();
            notifyObservers(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Start, just a convenience method.
     */
    public void start() {
        logger.finest("Init View");
        showOptions();
        getInput();
    }
}
