package nl.bannarte.grpc.client.service;

import com.google.common.util.concurrent.ListenableFuture;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import nl.bannarte.grpc.Author;
import nl.bannarte.grpc.Book;
import nl.bannarte.grpc.BookFinderServiceGrpc;
import nl.bannarte.grpc.BookList;

import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * The Client Service
 * <p>
 * Will do the actual communication with the server.
 *
 * @author Bart Kerkvliet
 * @since 1.0.0
 */
public class ClientService extends Observable implements Observer {
    /**
     * Logger used to generate log lines.
     */
    private static final Logger logger = Logger.getLogger(ClientService.class.getName());

    /**
     * The default host if none given.
     */
    private static final String DEFAULT_ADDRESS = "localhost";
    /**
     * The default port if none given.
     */
    private static final int DEFAULT_PORT = 1804;

    /**
     * Channel used to communicate.
     */
    private final ManagedChannel channel;
    /**
     * The blocking stub. Will block execution on data retrieval.
     */
    private final BookFinderServiceGrpc.BookFinderServiceBlockingStub blockingStub;
    /**
     * The asynchronous stub. Will not block execution of data retrieval.
     */
    private final BookFinderServiceGrpc.BookFinderServiceStub asyncStub;
    /**
     * Future stub. Uses a future, but can't use the stream.
     */
    private final BookFinderServiceGrpc.BookFinderServiceFutureStub futureStub;

    /**
     * Author used for searches.
     */
    private final Author author;
    /**
     * Is execution still going on or are we finished already?
     */
    private Boolean isDone;

    /**
     * Constructor using defaults.
     */
    public ClientService() {
        this(DEFAULT_ADDRESS, DEFAULT_PORT);
    }

    /**
     * Constructor using a custom port.
     *
     * @param port Port the server uses.
     */
    public ClientService(int port) {
        this(DEFAULT_ADDRESS, port);
    }

    /**
     * Constructor using a custom address.
     *
     * @param address Address of the server.
     */
    public ClientService(String address) {
        this(address, DEFAULT_PORT);
    }

    /**
     * Constructor using both a custom port and address.
     *
     * @param address Address of the server
     * @param port    Port of the server
     */
    public ClientService(String address, int port) {
        author = Author.newBuilder().setFirstName("Edsger Wybe").setLastName("Dijkstra").build();

        logger.fine("Creating Channel");
        channel = ManagedChannelBuilder.forAddress(address, port)
                .usePlaintext(true) // Disable the use of certificates, only use for testing purposes!
                .build();

        logger.finest("Creating Blocking Stub");
        blockingStub = BookFinderServiceGrpc.newBlockingStub(channel);
        logger.finest("Creating Asynchronous Stub");
        asyncStub = BookFinderServiceGrpc.newStub(channel);
        logger.finest("Creating Future Stub");
        futureStub = BookFinderServiceGrpc.newFutureStub(channel);
        logger.finest("Created Stubs");

        logger.info(String.format("Created stubs, connecting to: %s:%s", address, port));
    }

    /**
     * Shutdown the channel
     * <p>
     * Close the channel. This will prevent further communications with the server.
     *
     * @throws InterruptedException Exception if shutdown fails
     */
    public void shutdown() throws InterruptedException {
        logger.fine("Shutting down channel");
        channel.shutdown()
                .awaitTermination(5, TimeUnit.SECONDS);
        logger.info("Successfully shutdown channel");
    }

    /////////////////////////
    // BLOCKING
    /////////////////////////

    /**
     * Get a List of Books (BookList) using the blocking stub and get all at once.
     *
     * @return The books found as BookList
     */
    public BookList getBooksBlocking() {
        logger.finest("Get Books Blocking");
        return blockingStub.findBooksByAuthor(author);
    }

    /**
     * Get Books using the blocking stub as a stream.
     */
    public void getBooksBlockingAsStream() {
        logger.finest("Get Books Blocking As Stream");
        Iterator<Book> books = blockingStub.findBooksByAuthorAsStream(author);
        while (books.hasNext()) {
            Book book = books.next();
            logger.finest(String.format("Found book: %s", book));
            setChanged();
            notifyObservers(book);
        }
        logger.finest("No more books will follow");
    }

    /**
     * Get Books with an unknown author using the blocking stub.
     */
    public void getBooksBlockingNotFound() {
        logger.finest("Get Books Blocking, Unknown Author");
        Author unknownAuthor = Author.newBuilder().setFirstName("Bart").setLastName("Kerkvliet").build();
        try {
            blockingStub.findBooksByAuthor(unknownAuthor);
        } catch (StatusRuntimeException sre) {
            sre.printStackTrace();
        }
    }

    /**
     * Use the blocking stub with a deadline to get books as a stream
     */
    public void getBooksBlockingAsStreamDeadline() {
        logger.finest("Creating Stub with deadline");
        BookFinderServiceGrpc.BookFinderServiceBlockingStub blockingStubWithDeadline =
                blockingStub.withDeadlineAfter(1100L, TimeUnit.MILLISECONDS);

        Iterator<Book> books = blockingStubWithDeadline.findBooksByAuthorAsStream(author);
        try {
            while (books.hasNext()) {
                Book book = books.next();
                logger.finest(String.format("Found book: %s", book));
                hasChanged();
                notifyObservers(book);
            }
        } catch (StatusRuntimeException sre) {
            sre.printStackTrace();
        }
    }

    /**
     * Use the blocking stub with a deadline to get books, but not using a stream.
     */
    public void getBooksBlockingNonStreamDeadline() {
        logger.finest("Creating Stub with deadline");
        BookFinderServiceGrpc.BookFinderServiceBlockingStub blockingStubWithDeadline =
                blockingStub.withDeadlineAfter(1100L, TimeUnit.MILLISECONDS);
        try {
            BookList books = blockingStubWithDeadline.findBooksByAuthor(author);
            logger.info("Found: " + books);
        } catch (StatusRuntimeException sre) {
            sre.printStackTrace();
        }
    }

    /////////////////////////
    // ASYNC
    /////////////////////////

    /**
     * Use the asynchronous stub to get Books as a BookList
     */
    public void getBooksAsync() {
        asyncStub.findBooksByAuthor(author, new StreamObserver<BookList>() {
            @Override
            public void onNext(BookList value) {
                logger.fine(String.format("Received BookList: %s", value));
                setChanged();
                notifyObservers(value);
            }

            @Override
            public void onError(Throwable t) {
                t.printStackTrace();
                done();
            }

            @Override
            public void onCompleted() {
                logger.finest("All done");
                done();
            }
        });
    }

    /**
     * Use the asynchronous stub to get Books as a stream of books
     */
    public void getBooksAsyncAsStream() {
        asyncStub.findBooksByAuthorAsStream(author, new StreamObserver<Book>() {
            @Override
            public void onNext(Book value) {
                logger.fine(String.format("Received Book: %s", value));
                setChanged();
                notifyObservers(value);
            }

            @Override
            public void onError(Throwable t) {
                t.printStackTrace();
                done();
            }

            @Override
            public void onCompleted() {
                logger.finest("All done");
                done();
            }
        });
    }

    /**
     * Use the asynchronous stub with a deadline to get Books as a stream of books
     */
    public void getBooksAsyncAsStreamDeadline() {
        BookFinderServiceGrpc.BookFinderServiceStub asyncStubWithDeadline = asyncStub.withDeadlineAfter(1L, TimeUnit.MILLISECONDS);
        try {
            asyncStubWithDeadline.findBooksByAuthorAsStream(author, new StreamObserver<Book>() {
                @Override
                public void onNext(Book value) {
                    logger.fine(String.format("Received Book: %s", value));
                    setChanged();
                    notifyObservers(value);
                }

                @Override
                public void onError(Throwable t) {
                    t.printStackTrace();
                    done();
                }

                @Override
                public void onCompleted() {
                    logger.finest("All done");
                    done();
                }
            });
        } catch (StatusRuntimeException sre) {
            sre.printStackTrace();
        }
    }

    /////////////////////////
    // FUTURE
    /////////////////////////

    /**
     * Use the future stub to get books.
     *
     * @throws ExecutionException   Could be thrown by books.get
     * @throws InterruptedException Because of the sleep
     */
    public void getBooksAsyncAsFuture() throws ExecutionException, InterruptedException {
        ListenableFuture<BookList> books = futureStub.findBooksByAuthor(author);
        while (!books.isDone()) {
            Thread.sleep(500L);
            logger.info("Still receiving books");
        }
        logger.fine("Finished receiving books");
        BookList bookList = books.get();
        setChanged();
        notifyObservers(bookList);
    }

    /////////////////////////

    /**
     * Receive requests for execution here.
     *
     * @param o   the observable object.
     * @param arg an argument passed.
     * @see java.util.Observer
     * @see java.util.Observable
     */
    @Override
    public void update(Observable o, Object arg) {
        logger.fine("Received an update" + arg);
        if (arg instanceof String) {
            String str = (String) arg;
            switch (str) {
                case "1":
                    start();
                    BookList bookList = getBooksBlocking();
                    setChanged();
                    notifyObservers(bookList);
                    done();
                    break;
                case "2":
                    start();
                    getBooksBlockingAsStream();
                    done();
                    break;
                case "3":
                    start();
                    getBooksBlockingNotFound();
                    done();
                    break;
                case "4":
                    start();
                    getBooksBlockingNonStreamDeadline();
                    done();
                    break;
                case "5":
                    start();
                    getBooksBlockingAsStreamDeadline();
                    done();
                    break;
                case "6":
                    start();
                    getBooksAsync();
                    //done();
                    break;
                case "7":
                    start();
                    getBooksAsyncAsStream();
                    //done();
                    break;
                case "8":
                    start();
                    getBooksAsyncAsStreamDeadline();
                    //done();
                    break;
                case "9":
                    start();
                    try {
                        getBooksAsyncAsFuture();
                    } catch (ExecutionException | InterruptedException e) {
                        e.printStackTrace();
                    }
                    done();
                    break;
                case "0":
                    start();
                    try {
                        shutdown();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    setChanged();
                    notifyObservers("EXIT");
                default:
                    start();
                    System.out.println("Unable to process request");
                    done();
                    break;
            }
        }
    }

    /**
     * Start, set done to false and notify watchers.
     */
    private void start() {
        isDone = Boolean.FALSE;
        setChanged();
        notifyObservers(isDone);
    }

    /**
     * Set status to done and notify watchers execution is finished.
     */
    private void done() {
        isDone = Boolean.TRUE;
        setChanged();
        notifyObservers(isDone);
    }
}
