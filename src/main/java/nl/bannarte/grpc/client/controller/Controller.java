package nl.bannarte.grpc.client.controller;

import nl.bannarte.grpc.client.service.ClientService;
import nl.bannarte.grpc.client.view.CliView;

import java.util.logging.Logger;

/**
 * Start of the client
 * <p>
 * Not really a "Controller", but directly coupling the UI and service using the Observer pattern.
 *
 * @author Bart Kerkvliet
 * @version 1.0.0
 */
public class Controller {
    private static final Logger logger = Logger.getLogger(Controller.class.getName());

    /**
     * Start the client and attach observers
     *
     * @param args Optional arguments (not used)
     */
    public static void main(String[] args) {
        logger.fine("Starting Application");
        CliView view = new CliView();
        logger.finest("View created");
        ClientService grpcService = new ClientService();
        logger.finest("gRPC Service Created");
        view.addObserver(grpcService);
        logger.finest("Added Service to View");
        grpcService.addObserver(view);
        logger.finest("Added View to Service");
        logger.fine("Starting view");
        view.start();

        logger.finest("Getting into infinite loop");
        //noinspection InfiniteLoopStatement,StatementWithEmptyBody
        while (true) {
            // Empty to prevent the client from shutting down
        }
    }
}
