package nl.bannarte.grpc.server.dao;

import nl.bannarte.grpc.Author;
import nl.bannarte.grpc.Book;

import java.util.ArrayList;
import java.util.List;

import static nl.bannarte.grpc.BookType.*;

/**
 * Fake Backend
 * <p>
 * A fake backend with static content.
 *
 * @author Bart Kerkvliet
 * @since 1.0.0
 */
public class FakeBackend {
    /**
     * List of books.
     */
    private final List<Book> books;

    /**
     * Constructor, will fill the list of books.
     */
    public FakeBackend() {
        books = new ArrayList<>();
        books.add(Book.newBuilder().setTitle("A Discipline of Programming").setPublisher("Prentice Hall").setType(PAPERBACK).build());
        books.add(Book.newBuilder().setTitle("Selected Writings on Computing: A Personal Perspective").setPublisher("Springer-Verlag").setType(HARDCOVER).build());
        books.add(Book.newBuilder().setTitle("A Method of Programming").setPublisher("Pearson Education").setType(PAPERBACK).build());
        books.add(Book.newBuilder().setTitle("Predicate Calculus and Program Semantics").setPublisher("Springer-Verlag").setType(PAPERBACK).build());
        books.add(Book.newBuilder().setTitle("Primer of Algol 60 Programming (Studies in Data Processing)").setType(HARDCOVER).build());
        books.add(Book.newBuilder().setTitle("001: Current Trends in Programming Methodology, Vol. 1: Software Specification and Design").setType(EBOOK).build());
        books.add(Book.newBuilder().setTitle("Formal Development of Programs and Proofs").setPublisher("Pearson Education").setType(HARDCOVER).build());
    }

    /**
     * Simple "search" method.
     *
     * @param request Author to search by
     * @return List of books found
     */
    public List<Book> findBooksByAuthor(Author request) {
        Author author = Author.newBuilder().setFirstName("Edsger Wybe").setLastName("Dijkstra").build();
        if (request.equals(author)) {
            return books;
        }
        return new ArrayList<>();
    }
}
