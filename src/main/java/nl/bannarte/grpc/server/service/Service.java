package nl.bannarte.grpc.server.service;

import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import nl.bannarte.grpc.Author;
import nl.bannarte.grpc.Book;
import nl.bannarte.grpc.BookFinderServiceGrpc;
import nl.bannarte.grpc.BookList;
import nl.bannarte.grpc.server.dao.FakeBackend;

import java.util.List;
import java.util.logging.Logger;

/**
 * Service to call a backend
 * <p>
 * Calls a fake backend to present books.
 *
 * @author Bart Kerkvliet
 * @since 1.0.0
 */
public class Service extends BookFinderServiceGrpc.BookFinderServiceImplBase {
    /**
     * Logger used to generate log lines.
     */
    private static final Logger logger = Logger.getLogger(Service.class.getName());

    /**
     * Fake backend to get books from.
     */
    private final FakeBackend fakeBackend;

    /**
     * Constructor with default fake backend.
     */
    public Service() {
        this(new FakeBackend());
    }

    /**
     * Constructor set a custom fake (or real) backend.
     *
     * @param fakeBackend The backend to use
     */
    public Service(FakeBackend fakeBackend) {
        this.fakeBackend = fakeBackend;
    }

    /**
     * Find Books by author (BookList)
     *
     * @param request          Author to search by
     * @param responseObserver Send responses
     */
    @Override
    public void findBooksByAuthor(Author request, StreamObserver<BookList> responseObserver) {
        logger.finest(String.format("Fetching Books for: %s", request));
        List<Book> books = fakeBackend.findBooksByAuthor(request);

        if (books == null || books.size() == 0) {
            logger.finest("No books found");
            responseObserver.onError(noBooksFoundForAuthorException(request));
        } else {
            logger.finest("Building BookList");
            BookList.Builder builder = BookList.newBuilder();

            for (Book book : books) {
                delayServer();
                builder.addBooks(book);
                logger.finest(String.format("Added book: %s to BookList", book));
            }
            responseObserver.onNext(builder.build());
            logger.fine("Returned BookList");
        }
        responseObserver.onCompleted();
        logger.fine("No more books will follow.");
    }

    /**
     * Find Books by Author as a stream
     *
     * @param request          Author to search by
     * @param responseObserver Send responses
     */
    @Override
    public void findBooksByAuthorAsStream(Author request, StreamObserver<Book> responseObserver) {
        logger.finest(String.format("Fetching Books for: %s", request));
        List<Book> books = fakeBackend.findBooksByAuthor(request);

        if (books == null || books.size() == 0) {
            logger.finest("No books found");
            responseObserver.onError(noBooksFoundForAuthorException(request));
        } else {
            logger.finest("Returning Books");
            for (Book book : books) {
                delayServer();
                responseObserver.onNext(book);
                logger.fine(String.format("Returned book: %s", book));
            }
        }
        responseObserver.onCompleted();
        logger.fine("No more books will follow.");
    }

    /**
     * Slow the server down to emulate books coming in
     */
    private void delayServer() {
        try {
            logger.finest("Delay server by 250ms");
            Thread.sleep(250L);
        } catch (InterruptedException ie) {
            logger.severe("Unexpected exception");
            ie.printStackTrace();
        }
    }

    /**
     * Exeception to throw when no books are found.
     *
     * @param request The author to search.
     * @return Status with exception details
     */
    private StatusRuntimeException noBooksFoundForAuthorException(Author request) {
        return Status
                .NOT_FOUND
                .withDescription("No books found for author")
                .augmentDescription(String.format(
                        "No books found for author: %s This service can only return books written by E.W. Dijkstra", request
                ))
                .asRuntimeException();
    }
}
