package nl.bannarte.grpc.server;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import nl.bannarte.grpc.server.service.Service;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * The gRPC server
 * <p>
 * Handles requests for Books by Author.
 *
 * @author Bart Kerkvliet
 * @since 1.0.0
 */
public class BookLibraryServer {
    /**
     * Logger used to generate log lines.
     */
    private static final Logger logger = Logger.getLogger(BookLibraryServer.class.getName());

    /**
     * The default port used by the server.
     */
    private static final int defaultPort = 1804;

    /**
     * The server instance
     */
    private final Server server;

    /**
     * Constructor using the default port and service.
     */
    public BookLibraryServer() {
        this(defaultPort, new Service());
    }

    /**
     * Constructor, set an alternative port, but use default service.
     *
     * @param port The alternative port
     */
    public BookLibraryServer(int port) {
        this(port, new Service());
    }

    /**
     * Constructor to set an alternative service for the server.
     *
     * @param service The service to add.
     */
    public BookLibraryServer(Service service) {
        this(defaultPort, service);
    }

    /**
     * Constructor using a specified port and service.
     *
     * @param port    Port to use
     * @param service Service to use
     */
    public BookLibraryServer(int port, Service service) {
        logger.info("Creating server on port: " + port);
        server = ServerBuilder.forPort(port)
                .addService(service)
                .build();
    }

    /**
     * Start the server.
     *
     * @param args Optional arguments (Not used)
     * @throws InterruptedException Keep server alive failed
     * @throws IOException          Port is probably already in use
     */
    public static void main(String[] args) throws InterruptedException, IOException {
        logger.fine("Creating server");
        BookLibraryServer server = new BookLibraryServer();
        logger.finest("Server Created");
        logger.finest("Starting Server");
        server.start();
        logger.info("Server started");
        server.blockUntilShutdown();
    }

    /**
     * Start the service.
     * <p>
     * Add a hook to cleanly shutdown the server when the JVM gets shut down.
     *
     * @throws IOException Port might already be in use
     */
    public void start() throws IOException {
        server.start();
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            // Use stderr here since the logger may have been reset by its JVM shutdown hook.
            System.err.println("shutting down gRPC server since JVM is shutting down");
            BookLibraryServer.this.stop();
            System.err.println("server shut down");
        }));
    }

    /**
     * Stop the server.
     */
    public void stop() {
        if (server != null) {
            logger.fine("Shutting down server");
            server.shutdown();
            logger.info("Shutdown server");
        }
    }

    /**
     * Prevent the server from stopping
     *
     * @throws InterruptedException Could be thrown by awaitTermination
     */
    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }
}
